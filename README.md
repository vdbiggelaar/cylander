# Cylander - Point and Click Web Based Game

Web-based point and click game aimed at raising awareness about cybersecurity threats. The game allows players to interact with objects and solve puzzles to progress through different scenes.

## Note

This project is currently in development and is a first draft aimed at testing the technology and mechanics of the game. It is not a final product and may undergo significant changes and improvements in the future.


## Installation

1. Clone the repository: `git clone https://github.com/your-username/cylander.git`
2. Navigate to the project folder: `cd cylander`
3. Install dependencies: `npm install`

## How to Play

- Start the development server: `npm start`
- Click on objects to interact with them.

## Technologies Used

- [Phaser](https://phaser.io/): A fast and free open-source framework for Canvas and WebGL-powered browser games.
